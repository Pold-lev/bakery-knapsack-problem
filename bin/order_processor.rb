# frozen_string_literal: true

require 'optparse'
require 'json'
require_relative '../lib/bakery'
require_relative '../lib/bakery_initializer'
require_relative '../lib/order_builder'
require_relative '../lib/parcel_presenter'

bakery = Bakery.new

BakeryInitializer.call(
  bakery: bakery,
  config: YAML.load_file('./config/bakery_config.yml')
)

begin
  ARGF.each do |line|
    break if line.strip == 'exit'
    begin
      order_content = JSON.parse(line)

      if order_content.is_a? Hash
        order = OrderBuilder.call(order_content)
        parcel = bakery.build_parcel(order)

        ParcelPresenter.new(parcel).present
      else
        p 'Invalid input format'
      end
    rescue JSON::ParserError
      p 'Invalid input format'
    rescue BakeryError => e
      p "Error: #{e.message}"
    end
  end
rescue Interrupt
end
