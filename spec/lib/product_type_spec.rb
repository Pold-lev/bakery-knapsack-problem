require 'product_type'

describe ProductType do
  let(:product_type) { described_class.new(title: title, id_code: code) }
  let(:title) { 'TestType' }
  let(:code) { 'TT' }

  describe '#title' do
    it { expect(product_type.title).to eq 'TestType' }
  end

  describe '#id_code' do
    it { expect(product_type.id_code).to eq 'TT' }
  end

  describe '#packaging_options' do
    before { product_type.add_packaging_option(size: 2, price: 2.59) }

    it { expect(product_type.packaging_options.first).to be_a ProductType::PackagingOption }
    it { expect(product_type.packaging_options.first.size).to eq 2 }
    it { expect(product_type.packaging_options.first.price).to eq 2.59 }
  end

  describe '#price_for_pack_of' do
    before { product_type.add_packaging_option(size: 2, price: 2.59) }

    it { expect(product_type.price_for_pack_of(2)).to eq 2.59 }
  end
end
