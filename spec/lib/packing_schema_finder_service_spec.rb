require 'packing_schema_finder_service'

describe PackingSchemaFinderService do
  let(:finder) do
    described_class.new(packaging_options: options, target_total: target)
  end

  describe '#call' do
    context 'when packing schema exist' do
      let(:options) { [2, 3, 5] }
      let(:target) { 111 }

      it { expect(finder.call).to eq(3 => 2, 5 => 21) }
    end

    context 'when packing schema does not exist' do
      let(:options) { [2, 4] }
      let(:target) { 13 }

      it { expect(finder.call).to eq({}) }
    end
  end
end
