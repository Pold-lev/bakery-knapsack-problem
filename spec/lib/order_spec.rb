require 'order'
require 'order_item'

describe Order do
  let(:order) { Order.new(items) }
  let(:items) { [cf, bm11] }
  let(:cf) { OrderItem.new(product_code: 'CF', quantity: 13) }
  let(:bm11) { OrderItem.new(product_code: 'BM11', quantity: 12) }

  describe '#items' do
    it { expect(order.items).to contain_exactly cf, bm11 }
  end
end
