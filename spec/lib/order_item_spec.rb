require 'order_item'

describe OrderItem do
  let(:order_item) { OrderItem.new(product_code: product_code, quantity: quantity) }
  let(:product_code) { 'CF' }
  let(:quantity) { 13 }

  describe '#product_code' do
    it { expect(order_item.product_code).to eq 'CF' }
  end

  describe '#quantity' do
    it { expect(order_item.quantity).to eq 13 }

    context 'when quantity not integer' do
      let(:quantity) { 'not-integer-at-all' }
      it { expect { order_item }.to raise_error ArgumentError }
    end
  end
end
