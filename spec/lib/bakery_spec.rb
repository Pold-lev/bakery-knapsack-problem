require 'bakery'
require 'product_type'

describe Bakery do
  let(:bakery) { described_class.new }

  describe '#add_product_type' do
    let(:product_type) { ProductType.new(title: 'Test', id_code: 'T') }

    context 'when add new product type' do
      it { expect { bakery.add_product_type(product_type) }.not_to raise_error }
    end

    context 'when product type with such code alredy exists' do
      before { bakery.add_product_type(product_type) }

      it do
        expect { bakery.add_product_type(product_type) }
          .to raise_error Bakery::ProductTypeDuplicationError
      end
    end
  end

  describe '#build_parcel' do
    let(:order) { Order.new(order_items) }
    let(:order_items) { [order_item1, order_item2] }


    let(:product_type1) do
      ProductType.new(title: 'Test1', id_code: 'T1').tap do |type|
        type.add_packaging_option(size: 2, price: 3.80)
      end
    end

    let(:product_type2) do
      ProductType.new(title: 'Test2', id_code: 'T2').tap do |type|
        type.add_packaging_option(size: 1, price: 2.50)
        type.add_packaging_option(size: 2, price: 3.80)
      end
    end

    before do
      bakery.add_product_type(product_type1)
      bakery.add_product_type(product_type2)
    end

    context 'when known product types' do
      let(:order_item1) { OrderItem.new(product_code: 'T1', quantity: 2) }
      let(:order_item2) { OrderItem.new(product_code: 'T2', quantity: 3) }

      it { expect(bakery.build_parcel(order)).to be_a Parcel }
      it do
        expect(bakery.build_parcel(order)).to have_attributes(
          items: contain_exactly(
            {
              product_code: 'T1',
              packing_info: [
                {pack_size: 2, count: 1, subtotal: 3.8}
              ]
            },
            {
              product_code: 'T2',
              packing_info: [
                {pack_size: 1, count: 1, subtotal: 2.5},
                {pack_size: 2, count: 1, subtotal: 3.8}
              ]
            }
          )
        )
      end
    end

    context 'when product type unknown' do
      let(:order_item1) { OrderItem.new(product_code: 'T1', quantity: 2) }
      let(:order_item2) { OrderItem.new(product_code: 'UNKNOWN', quantity: 3) }

      it do
        expect { bakery.build_parcel(order) }.to raise_error Bakery::UnknownProductTypeError
      end
    end

    context 'when packing impossible' do
      let(:order_item1) { OrderItem.new(product_code: 'T1', quantity: 3) }
      let(:order_item2) { OrderItem.new(product_code: 'T2', quantity: 3) }

      it do
        expect { bakery.build_parcel(order) }.to raise_error Bakery::NoPackingOptionsError
      end
    end
  end
end
