require 'bakery_initializer'
require 'bakery'
require 'product_type'

describe BakeryInitializer do
  let(:initializer_call) { described_class.call(bakery: bakery, config: config) }
  let(:bakery) { spy }
  let(:config) do
    {
      'product_types' => [
        'title' => 'TestTitle',
        'id_code' => 'TT',
        'packs' => [
          {
            'size' => 3,
            'price' => 5.99
          },
          {
            'size' => 5,
            'price' => 8.99
          }
        ]
      ]
    }
  end

  describe '.call' do
    it do
      expect(bakery).to receive(:add_product_type).with(
        have_attributes(
          title: 'TestTitle',
          id_code: 'TT',
          packaging_options: [
            ProductType::PackagingOption.new(3, 5.99),
            ProductType::PackagingOption.new(5, 8.99)
          ]
        )
      )

      initializer_call
    end
  end
end
