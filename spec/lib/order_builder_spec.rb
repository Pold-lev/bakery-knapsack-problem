require 'order_builder'
require 'order'

describe OrderBuilder do
  let(:oder_builder) { described_class }
  describe '.call' do
    let(:builder_call) { oder_builder.call(order_content) }

    context 'when order content nil' do
      let(:order_content) { nil }
      it { expect { builder_call }.to raise_error OrderBuilder::EmptyOrderError }
    end

    context 'when order content empty' do
      let(:order_content) { {} }
      it { expect { builder_call }.to raise_error OrderBuilder::EmptyOrderError }
    end

    context 'when order content present' do
      context 'when order content has invalid order quantity' do
        let(:order_content) { {CF: 'qwe'} }
        it { expect { builder_call }.to raise_error OrderBuilder::NonIntegerQuantity }
      end

      context 'when valid order content' do
        let(:order_content) { {'CF' => 13} }
        it { expect(builder_call).to be_a Order }
        it { expect(builder_call.items.first).to have_attributes(product_code: 'CF', quantity: 13) }
      end
    end
  end
end
