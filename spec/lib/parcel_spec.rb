require 'parcel'

describe Parcel do
  let(:parcel) { described_class.new }

  describe '#items' do
    it { expect(parcel.items).to be_empty }
  end

  describe '#add_package' do
    let(:add_package) do
      parcel.add_package(product_type: product_type, packing_schema: packing_schema)
    end

    let(:product_type) do
      ProductType.new(title: 'Test', id_code: 'T1').tap do |type|
        type.add_packaging_option(size: 2, price: 2.59)
      end
    end

    let(:packing_schema) { {2 => 4} }
    it do
      expect { add_package }.to change { parcel.items }.from([]).to(
        [
          {
            product_code: 'T1',
            packing_info: [
              {
                pack_size: 2,
                count: 4,
                subtotal: 10.36
              }
            ]
          }
        ]
      )
    end
  end
end
