require 'yaml'
require_relative 'product_type'

class BakeryInitializer
  class << self
    def call(bakery:, config:)
      config['product_types'].each do |product_type_config|
        bakery.add_product_type(build_product_type(product_type_config))
      end
    end

    private

    def build_product_type(config)
      product_type =
        ProductType.new(title: config['title'], id_code: config['id_code'])

      product_type.tap do |type|
        config['packs'].each do |pack_config|
          type.add_packaging_option(
            size: Integer(pack_config['size']),
            price: Float(pack_config['price'])
          )
        end
      end
    end
  end
end
