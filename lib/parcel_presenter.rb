class ParcelPresenter
  def initialize(parcel)
    @parcel = parcel
  end

  def present
    @parcel.items.each do |item|
      present_item(item)
    end
  end

  private

  def present_item(item)
    puts "#{total_quantity(item)} #{item[:product_code]} $#{total_price(item)}"
    item[:packing_info].each { |pack_info| present_pack(pack_info) }
  end

  def total_quantity(item)
    item[:packing_info].reduce(0) do |total_count, package|
      total_count + (package[:count] * package[:pack_size])
    end
  end

  def total_price(item)
    item[:packing_info].map do |pack_info|
      pack_info[:subtotal]
    end.reduce(&:+).round(2)
  end

  def present_pack(pack_info)
    count = pack_info[:count]
    pack_size = pack_info[:pack_size]
    subtotal = pack_info[:subtotal].round(2)
    puts "      #{count} x #{pack_size} $#{subtotal}"
  end
end
