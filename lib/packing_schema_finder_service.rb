class PackingSchemaFinderService
  def initialize(packaging_options:, target_total:)
    @packaging_options = packaging_options.sort.reverse!
    @target_total = target_total
    @optimal_packages =
      Array.new(target_total + 1) { Hash.new { |hash, key| hash[key] = 0 } }
  end

  def call
    (@packaging_options.last..@target_total).each do |order_size|
      find_optimal_for_order_size(order_size)
    end

    @optimal_packages.last
  end

  private

  def find_optimal_for_order_size(order_size)
    if @packaging_options.include?(order_size)
      @optimal_packages[order_size][order_size] = 1
    else
      @packaging_options.each do |pack_size|
        next if pack_size > order_size

        new_optimal = find_optimal_for_pack_size(order_size, pack_size)
        @optimal_packages[order_size] = new_optimal unless new_optimal.nil?
      end
    end
  end

  def find_optimal_for_pack_size(order_size, pack_size)
    optimal_package_candidates = [@optimal_packages[order_size]]

    complementary_smaller_pack = @optimal_packages[order_size - pack_size]

    unless complementary_smaller_pack.empty?
      optimal_package_candidates <<
        complementary_smaller_pack.dup
          .tap { |package| package[pack_size] += 1 }
    end

    optimal_package_candidates.reject!(&:empty?)

    return if optimal_package_candidates.empty?

    optimal_package_candidates.min_by { |pack| pack.values.reduce(&:+) }
  end
end
