class Parcel
  attr_reader :items

  def initialize
    @items = []
  end

  def add_package(product_type:, packing_schema:)
    @items << {
      product_code: product_type.id_code,
      packing_info: packing_schema.map do |pack_size, count|
        {
          pack_size: pack_size,
          count: count,
          subtotal: count * product_type.price_for_pack_of(pack_size)
        }
      end
    }
  end
end
