require_relative 'packing_schema_finder_service'
require_relative 'parcel'
require_relative 'bakery_error'

class Bakery
  ProductTypeDuplicationError = Class.new(BakeryError)
  UnknownProductTypeError = Class.new(BakeryError)
  NoPackingOptionsError = Class.new(BakeryError)

  def initialize
    @product_types = {}
  end

  def add_product_type(product_type)
    unless @product_types[product_type.id_code].nil?
      raise ProductTypeDuplicationError, 'Product type already exsist'
    end

    @product_types[product_type.id_code] = product_type
  end

  def build_parcel(order)
    parcel = Parcel.new

    order.items.each do |order_item|
      product_type = @product_types[order_item.product_code]

      raise UnknownProductTypeError, 'Unknown product type' if product_type.nil?

      packing_schema = PackingSchemaFinderService.new(
        packaging_options: product_type.packaging_options.map(&:size),
        target_total: order_item.quantity
      ).call

      raise NoPackingOptionsError, "Can't pack order" if packing_schema.empty?
      parcel.add_package(product_type: product_type, packing_schema: packing_schema)
    end

    parcel
  end
end
