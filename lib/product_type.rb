class ProductType
  attr_reader :title, :id_code
  PackagingOption = Struct.new(:size, :price)

  def initialize(title:, id_code:)
    @title = title
    @id_code = id_code
    @packaging_options = {}
  end

  def add_packaging_option(size:, price:)
    @packaging_options[size] = PackagingOption.new(size, price)
  end

  def packaging_options
    @packaging_options.values
  end

  def price_for_pack_of(size)
    @packaging_options[size].price
  end
end
