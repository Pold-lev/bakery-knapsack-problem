class OrderItem
  attr_reader :product_code, :quantity
  def initialize(product_code:, quantity:)
    @product_code = product_code
    @quantity = Integer(quantity)
  end
end
