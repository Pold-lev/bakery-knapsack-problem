require_relative 'order'
require_relative 'order_item'
require_relative 'bakery_error'

class OrderBuilder
  EmptyOrderError = Class.new(BakeryError)
  NonIntegerQuantity = Class.new(BakeryError)

  def self.call(order_content)
    if order_content.nil? || order_content.empty?
      raise EmptyOrderError, 'Order has no items'
    end

    order_items = order_content.map do |code, quantity|
      unless quantity.is_a? Integer
        raise NonIntegerQuantity, 'Quantity should be integer'
      end
      OrderItem.new(product_code: code, quantity: quantity)
    end

    Order.new(order_items)
  end
end
