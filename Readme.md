# Bakery Coding Challenge
##[Task](Coding%20Challenge.pdf)

## Requirements
- Ruby = 2.3

## Setup
In project directory run:
- run `bundle install`

## Configuration
Bakery products and pack configurations stored in `config/bakery_config.yml`

## Usage
- run `ruby bin/order_processor.rb`
- enter order info hash in json format

## Examples
```
echo '{"CF":13, "MB11":14, "VS5":10}' | ruby bin/order_processor.rb
13 CF $25.85
      1 x 3 $5.95
      2 x 5 $19.9
14 MB11 $54.8
      3 x 2 $29.85
      1 x 8 $24.95
10 VS5 $17.98
      2 x 5 $17.98
```

## Specs
`rspec .` to run specs
